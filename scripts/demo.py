import ROOT
ROOT.TTreeReader # just to be sure the corresponding lib is loaded
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

from BinnedPhaseSpace.BinnedPhaseSpaceConfig import h1 ,h2
from JITAnalysis.JITAnalysisConfig import MainLoop, jitConfig, BinnedHistoTask



testFile = "../../../Data/Mass/user.delsart.301331.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.muDAODmass1.e4061_s2608_s2183_r7772_r7676_p2794_EXT0/user.delsart.9790734.EXT0._all.DAOD_JETM8.muDAODmass1.pool.root"


# ********************************************
# setup the main loop with the input file.
mainL = MainLoop()
mainL.m_chain.SetName("CollectionTree")
mainL.addFile( testFile , 1.)



# ********************************************
# Add histogramming tasks
#

# jets in our testfiles :
prefix='AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux'
prefixT='AntiKt10TruthTrimmedPtFrac5SmallR20Jets2Aux'

mainL.addHisto( prefix+'.pt', (500,0,3000000 ), name='jetpt')
mainL.addHisto( prefix+'.pt', (500,0,3000000 ), name='leadjetpt', index=0) # leading jet !
mainL.addHisto( prefixT+'.pt', (500,0,3000000 ), name='leadtruthjetpt', index=0) # truth leading jet !

# h1() is a helper function to ensure the binning info is correctly formated (and can deal with lists).
# not strictly necessary but helpful.
mainL.addHisto( prefix+'.m', h1(500,0,500000 ), name='jetm')

# Select only some of the jets (those with pt>100000) : 
mainL.addHisto( prefix+'.m', h1(500,0,500000 ), sel=prefix+'.pt>100000', name='jetm_highpt',
                LineColor=ROOT.kRed,       # and set the histo line color !
                XTitle="jet mass [GeV]", ) # and the xaxis title... any TH1F::SetBLABLA can be passed.


# one can use any function known to ROOT :
mainL.addHisto( 'TMath::Log(%s.m/%s.pt)'%(prefix,prefix), h1(500,-30,0 ), name='jetlogm_o_pt')

mainL.addHisto( prefix+'Dyn.pttruthResp', h1(200,0,4), name='jetptresp') # an other variable in the tree

mainL.addHisto( 'event.totalWeight', h1(100,0, 3 ), name='eventWeight') # the system defines an EventInfo object accessible from here

mainL.addHisto( 'float a=1.03; event.totalWeight*a', h1(100,0, 3 ), name='eventWeightscaled') # the system defines an EventInfo object accessible from here


# 2D histo :
mainL.addHisto( prefix+'.pt:'+prefix+'.m', h2(200,0,400000,100,0,3000000 ), name='jetptvsm')

# Use a selection : plot only if the expression passed to 'sel' evaluates to true :
mainL.addHisto( prefix+'Dyn.pttruthResp', h1(200,0,4), sel=prefix+'Dyn.pttruthResp>1.',name='jetptresp_resp1') #

mainL.m_useMcEventWeight = False

# encapsulate the run and save in a function just in case we want to comment it out easily
def runAndSave(nevt):
    mainL.loop(nevt)
        
    mainL.saveOutput( "out.root" )


runAndSave(-1)

