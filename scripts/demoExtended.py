import ROOT
ROOT.TTreeReader # just to be sure the corresponding lib is loaded
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

from BinnedPhaseSpace.BinnedPhaseSpaceConfig import h1 ,h2
from JITAnalysis.JITAnalysisConfig import MainLoop, jitConfig, BinnedHistoTask

testFile = "../../../Data/Mass/user.delsart.301331.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.muDAODmass1.e4061_s2608_s2183_r7772_r7676_p2794_EXT0/user.delsart.9790734.EXT0._all.DAOD_JETM8.muDAODmass1.pool.root"


# define an extended EventInfo
ROOT.gInterpreter.LoadText("""
struct MyEventInfo : public JITA::EventInfo {
  float aa = 0.2;
  float bb = 0;
};
""")


# define a new Task :
ROOT.gInterpreter.LoadText('''
class MyTask : public JITA::AnalysisTask {
public:
//using JITA::AnalysisTask::AnalysisTask; "using" does not work in python ???
MyTask(const std::string & n) : JITA::AnalysisTask(n) {}
virtual void processEvent( ) { MyEventInfo & event = static_cast<MyEventInfo&>(*m_event);
                               event.aa = event.aa*3.823*(1-event.aa);}
};
''')
# the above extended classes could also have been put in proper .h/.cxx files in the package and compiled
# in the package dictionnary (Root/LinkDef.h)
# This would avoid a recompilation each time we start this script.


# next we want a Task which makes use of some variable of the TTree:
# We also need to nmake it inherit CalculatorBase.
taskAccessingBranchDef='''
class TaskAccessingBranch : public JITA::AnalysisTask , public JITA::CalculatorBase {
public:
//using JITA::AnalysisTask::AnalysisTask; "using" does not work in python ???
TaskAccessingBranch() : JITA::AnalysisTask("Taskaccessingbranch") {}
virtual void initialize( JITA::RunInfo&r ) {CalculatorBase::initialize(r);} // IMPORTANT !!!
virtual void initEvent( ) {CalculatorBase::initEvent();} // IMPORTANT !!!
virtual void processEvent( ) { MyEventInfo & event = static_cast<MyEventInfo&>(*m_event);
                               if(  AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux.eta.size()>0 )
                                  event.bb = AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux.eta[0];
                               else
                                  event.bb = 10;
                               }
};
'''
# we delay the compilation to later  because the system will have to know
# how to interpret the branch name.

mainL = MainLoop()
mainL.m_chain.SetName("CollectionTree")
mainL.addFile( testFile , 1.)
jitConfig.setTree( mainL.m_chain )

# use the extendend class in place of the default EventInfo object :
mainL.setEvent( ROOT.MyEventInfo() )


# jets in our testfiles :
prefix='AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux'
prefixT='AntiKt10TruthTrimmedPtFrac5SmallR20Jets2Aux'

# ***********************************
# Basic histos
mainL.addHisto( prefix+'.pt', h1(500,0,3000000 ), name='jetpt')
mainL.addHisto( prefix+'.pt', h1(500,0,3000000 ), name='leadjetpt', index=0) # leading jet !
mainL.addHisto( prefixT+'.pt', h1(500,0,3000000 ), name='leadtruthjetpt', index=0) # truth leading jet !

mainL.addHisto( prefix+'.m', h1(500,0,500000 ), name='jetm')

mainL.addHisto( prefix+'Dyn.pttruthResp', h1(200,0,4), name='jetptresp') # an other variable in the tree


# ***********************************
# use the extended classes
mainL.add( ROOT.MyTask("mytask") ) # adds our own task
mainL.addHisto( 'event.aa', h1(100,0, 1 ), name='aa') # we can now access our MyEventInfo object

# now we can compile our other task into a proper task instance :
mainL.add( jitConfig.compileFullCalcObject( "TaskAccessingBranch", taskAccessingBranchDef )  ) 
# this tasks modifies event.bb . So let's add a new histos to see this :
mainL.addHisto( 'event.bb', h1(100,-5, 11 ), name='bbHisto') # we can now access our MyEventInfo object


# ***********************************
# 2D histo :
mainL.addHisto( prefix+'.pt:'+prefix+'.m', h2(200,0,400000,100,0,3000000 ), name='jetptvsm')




# ***********************************
# Histogramming in bins of some variable
#
# We want to plot a few variables/calculations by bin of jet pT
# First we describe a binned space :

from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, vectorFloat,h1, BinnedOperations,h2
ptBins =BinnedPhaseSpace("ptBins",
                         AxisSpec("pt", "p_{T}",[200000.0, 500000, 1000000.0, 1500000, 2000000, 5000000]),
                         # format is : AxisSpec( tag , title, [bin0_lowedge, bin1_lowedge, ..., binN_highEdge] )
                         #        or   AxisSpec( tag , title, nbin, min, max, ...other options... )
                         # we can add up to 4 dimensions
                         # for example to add 4 bins in eta 
                         #AxisSpec("eta", "#eta",4, -2,2,isGeV=False),                               
                         )

# Next we prepare a task dedicated to fill histos according to this bin space
#  (after this, we will define what histos we to bin)
binFiller = BinnedHistoTask( "ptBins",
                             ptBins,  # the bin space we want to fill
                             # we specify the TTree variable names which corresponds to the bins in our bin space
                             [ "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux.pt" ] ,# add as many names as there are dimensions in the space.
                             sel= "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets2Aux.m>45000",
                             nInstMax = 1 # set how many jets per event we will bin and fill histos with
                             )
# Now we define histos we want to fill in each bin :
# pt response :
binFiller.add( prefix+'Dyn.pttruthResp', h1(100,0,3), name="ptResp", XTitle="pT response" )
# for each bin of the bin space a 'ptResp' histogram will be created and filled 

#binFiller.add( prefix+'.m', h1(100,0,300000), name="m", XTitle="pT response", )#sel=prefix+".m>50000" ) just for debugging


# pt response for massive jets (use the 'sel' option to define a selection expression) :
binFiller.add( prefix+'Dyn.pttruthResp', h1(100,0,3), name="ptRespM50", XTitle="pT response when mass>50GeV", sel='('+prefix+'.m>50000)' )

# we add our bin filler to the main loop :
mainL.add( binFiller )



def runAndSave(nevt):
    mainL.loop(nevt)
        
    mainL.saveOutput( "out.root" )


runAndSave(-1)

