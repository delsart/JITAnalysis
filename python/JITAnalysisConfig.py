import ROOT
from array import array
from BinnedPhaseSpace.BinnedPhaseSpaceConfig import HistoDesc, h1, setHistoProp


def sanitizeName(name):
    return name.replace('.','_').replace(':','_vs_').replace(' ','_').replace('/','_ov_')

def setHistoProp(h, hprop):
    for k,v in hprop.iteritems():
        getattr(h,'Set'+k)(v)

def _decorateClass(klass, funcs):
    for f in funcs:
        setattr(klass, f.__name__, f)

def _replaceClassFunc(klass, func):
    old = getattr(klass, func.__name__)
    setattr(klass, func.__name__, func)
    setattr(klass, func.__name__+'orig', old)

def addBracket(expr, rep):
    """ adds [i] if needed after each occurance of rep in expr.
    this is recursive
    """
    N = len(rep)
    i=expr.find(rep)
    if i == -1 : # not found : do nothing.
        return expr
    # if 'rep' is not in the form 'rep[ bla ]' or 'rep.somefunc', replace it by 'rep[i]'
    if i+N >= len(expr) or (expr[i+N] != '[' and expr[i+N] != '.') :
        return expr[:i+N]+'[i]'+addBracket(expr[i+N:],rep)
    else:
        return expr[:i+N]+addBracket(expr[i+N:],rep)


class VariableMaker(object):
    """Helper to transform c++ expressions in which TTree branch names appear and replace them by array names so
    that the expression is compilable in the context of a CalculatorBase.
    Each instance is dedicated to a given branch/array type.
    """
    def __init__(self,typ, arrayName, varAccessCode):
        self.typ = typ
        self.arrayName = arrayName
        self.varAccessCode= varAccessCode

        def scalarReplace(expr, vname, i):
            rep = varAccessCode%(i,)
            expr = expr.replace(vname, rep)
            return expr
        def vectorReplace(expr, vname, i):
            rep = varAccessCode%(i,)
            expr = expr.replace(vname, rep)
            expr = addBracket( expr, rep)
            return expr

        self.replacer = vectorReplace if 'vector' in typ else scalarReplace
        self.scalarReplacer = scalarReplace

    def replaceBranchNames(self, expr, branchesPerType, noBracket=False):
        """Given expr, returns a string where each branch name in branchesPerType[self.typ] is replaced by a c++ expression corresponding to 1 CalculatorBase variable.
          details of what is replaced is depends on the type of this helper and implemented in self.replacer.
        """
        listOfNames =  branchesPerType[self.typ]

        thisReplacer = self.replacer
        if noBracket :
            thisReplacer = self.scalarReplacer

        for i,vname in enumerate(listOfNames):
            expr = thisReplacer( expr, vname, i)
        return expr

    def assignBranchNames(self, calc, branchesPerType):
        """Add all branches found in branchesPerType into the calc instance"""
        # retrieve the vector<string> corresponding to this type :
        vec = getattr( calc, self.arrayName+'BrNames')
        listV = branchesPerType[self.typ]
        for vname in listV:
            vec.push_back( vname )


class JITBuilder(object):
    """This helper class allows to translate a given C++ expresion into a class inheriting CalculatorBase. """

    ## The base class from which we JIT compile the calculator instances
    ##  It must inherit JITA::CalculatorBase
    calcBaseClass = "JITA::CalculatorBase"

    ## The EventInfo class used by the calculators.
    ##  It must inherit JITA::EventInfo
    eventClass = "JITA::EventInfo"

    ## This is the calculator class to be JITed from the expression in {code}
    calcTemplate = """class {cname} : public {bname} {{
    {evtclass}* m_event;
    void initialize(JITA::RunInfo &r){{ m_event = static_cast<{evtclass}*>(r.event);  {bname}::initialize(r);}}
    float calculate(int i){{ auto& event=*m_event;  {code} }} }};    // std::cout<< " {cname} "<< this <<std::endl; 
    """

    knownCalculators = {}
    knownClasses = set()
    
    tree = None
    branchDict = {}




    supportedTypes = [
        #              type of branch ,  array of var,   c++ expr to replace the branch name 
        VariableMaker( 'vector<float>' ,  'm_af'   ,'(*m_af[%d])'),
        VariableMaker( 'vector<double>' , 'm_ad'   ,'(*m_ad[%d])'),
        VariableMaker( 'vector<int>'   ,  'm_ai'   ,'(*m_ai[%d])'),
        VariableMaker( 'float'         ,  'm_sf'   ,'(m_sf[%d]->get())'),
        VariableMaker( 'double'         , 'm_sd'   ,'(m_sd[%d]->get())'),
        VariableMaker( 'int'           ,  'm_si'   ,'(m_si[%d]->get())'),                       
        #VariableMaker( 'unsigned int'  , 'm_si'   ,'(m_si[%d]->get())'),                       
        ]

    def __init__(self):
        pass

    def buildBranchTypeDict(self):
        branchDicPerType = { 'vector<float>' : set() ,'vector<int>' : set() , 'float' : set(), 'int' : set(),
                             'vector<double>' : set(), 'double' : set()}

        branchDicPerType['Float_t'] = branchDicPerType['float']
        branchDicPerType['Int_t'] = branchDicPerType['int']
        return branchDicPerType
    
    def setTree(self, tree):
        if tree == self.tree:
            return
        leaves = [b.GetListOfLeaves()[0] for b in tree.GetListOfBranches()]
        self.branchDict  =dict ( (b.GetName(), b.GetTypeName() ) for b in leaves )
        self.tree = tree
        
    def buildCalculator(self, expr, calcBaseClass=None):
        """Generate and compile a class derived from self.calcBaseClass and returns an instance of it.
        expr will be used as the code for the calculate() method to be generated.
        Any name in expr corresponding to an existing TBranch of self.tree will be replaced by a variable pointing to this branch.
        The remaining of expr is left unchanged.
        expr must be a valid C++ expression and can include any function known to the ROOT
        interperter.
        expr can also contain several ';' separated expressions (but no ';' at the end). Ex : 'double a=func(branch_var1); a*branch_var2'

        Usage of vectors.
        Say branch 'v1' contains a vector<T> (T=int or float). Then :
          * 'v1' is replaced by 'v1[i]' where i is the argument passed to CalculatorBase::calculate( i ).
          * 'v1[X] is left unchanged. X can be a litteral (ex : 'v1[0]') but then this does not guarantee the calculator will be called only once (contrary to what TTree::Draw('v1[0]') )
                                      X can also be any valid c++ expr defined in the calculator.        
                                  
        """

        if not isinstance(expr , str):
            return expr # assuming it's already a calculator base

        if calcBaseClass is None :
            calcBaseClass = self.calcBaseClass

        # remove white spaces :
        expr.strip()
        expr = ' '.join(expr.split())

        # check this calculator is not already known.
        if expr in self.knownCalculators:
            return self.knownCalculators[expr]

        compiled_expr, branchesPerType, isPureScalar = self.toCompilableExpr( expr )

        # finalize the code, adding return and ;
        tmpL = compiled_expr.split(';')
        tmpL[-1] = 'return '+tmpL[-1]+';'

        code = ';'.join( tmpL )

        # class name
        className = 'Calc_'+str(abs(hash(code)))

        if className not in self.knownClasses:
            self.knownClasses.add(className)
            print " generating ", className, 'for ', compiled_expr
            # now jit the expr :
            code = self.calcTemplate.format( bname=self.calcBaseClass, cname=className, code=code, evtclass=self.eventClass)
            res=ROOT.gInterpreter.LoadText(code) # much faster in ROOT 6.08 ??
            print ' compilation result = ', res , className
            if not bool(res) :
                print "compilation of "
                print code
                print " Failed !"
                return

        # instantiate the object :
        calc = getattr(ROOT, className)()

        # assign branch names :
        for varMaker in self.supportedTypes:
            varMaker.assignBranchNames( calc, branchesPerType)
        
        calc.setPureScalar(isPureScalar)

        self.knownCalculators[ expr ] = calc

        calc.expr = expr
        calc.code = code
        #print '____________________ xxx ', calc
        return calc


    def compileFullCalcObject( self , className, expr ):
        code, branchesPerType, isPureScalar = self.toCompilableExpr( expr , noBracket=True )

        if className not in self.knownClasses:
            self.knownClasses.add(className)
            print " generating ", className, 'for ', code
            res=ROOT.gInterpreter.LoadText(code) # much faster in ROOT 6.08 ??
            if not bool(res) :
                print "compilation of "
                print code
                print " Failed !"
                return
        # instantiate the object :
        calc = getattr(ROOT, className)()

        # assign branch names :
        for varMaker in self.supportedTypes:
            varMaker.assignBranchNames( calc, branchesPerType)
        
        calc.setPureScalar(isPureScalar)

        calc.expr = expr
        calc.code = code
        return calc



    def toCompilableExpr(self, expr, noBracket=False):
        """Within expr find  variables corresponding to branch names and replace them by compilable
        expressions (namely the member of CalculatorBase like '(*m_af[0])'
        return the compilable expression and a branchesPerType dict in the format { 'c++type' : [list of branches] }
        """
        print 'compiling ',expr
        isPureScalar = True
        # Try to match each variable in expr to a branch name.
        # collect each branch found per type.
        branchesPerType = self.buildBranchTypeDict()
        from re import finditer
        for match in finditer('[\w\.]+(\(|\[)?',expr): # search var name like "AA11.BB22" or "AA11.BB22[" or "AA11.BB22("
            v = match.group()
            vectorMethodCall = False
            if v.endswith('('): 
                if v.endswith('size('):
                    v = v[:-6] # remove 'size('
                    vectorMethodCall = True
                else:
                    # an other func. We won't even try to match it to a branch variable
                    continue
            if v.endswith('['):
                v = v[:-1]

            #typ = self.branchDict.get( v , None )
            try:
                typ = self.tree.GetBranch( v ).GetListOfLeaves()[0].GetTypeName()
                isVector = typ.startswith('vector') and not vectorMethodCall
                isPureScalar = isPureScalar and not isVector
                print '  found branch ',v , typ
                branchesPerType[ typ ].add( v )
            except Exception as e :
                print e,' ---- unknown ', v

        # freeze the set of used variables into ordered list:
        for t, setofVar in branchesPerType.iteritems():
            branchesPerType[t] = list(setofVar)

        # replace each branch by a member of the CalculatorBase class
        # so we get an expression that can be compiled
        compiled_expr = expr
        for varMaker in self.supportedTypes:
            compiled_expr= varMaker.replaceBranchNames( compiled_expr , branchesPerType, noBracket)
        
        return compiled_expr , branchesPerType, isPureScalar
    
jitConfig = JITBuilder()




        

## **********************************************
## Augmenting AnalysisTask
## **********************************************
def saveHistos(self,f):
    pass

def histos(self):
    return []

_decorateClass(ROOT.JITA.AnalysisTask, [saveHistos, histos] )




## **********************************************
## Augmenting HistoTask
## **********************************************
def __init__(self, name, expr, hspec, sel=None,  **args):
    ROOT.JITA.HistoTask.__init__orig(self,name)
    klass=args.pop('calcBaseClass',None)
    index=args.pop('index', -1)
    hfiller = histoFillerFromExpr( expr, hspec, sel=sel, name=name, fixedIndex=index  , calcBaseClass=klass, **args)
    setHistoProp(hfiller.histo(), args)
    self.m_filler = hfiller
    self._py_hfiler = hfiller

def saveHistos(self, f):
    f.cd()
    #print 'saving ', self._py_hfiler._py_h , self._py_hfiler._py_h.GetLineColor()
    self._py_hfiler._py_h.Write()

def histos(self):
    return [self._py_hfiler._py_h]
    

_decorateClass(ROOT.JITA.HistoTask, [saveHistos, histos] )
_replaceClassFunc(ROOT.JITA.HistoTask, __init__)



## **********************************************
## Augmenting BinnedHistoTask
## **********************************************
BinnedHistoTask = ROOT.JITA.BinnedHistoTask
def __init__(self, name, bps, calcExpr=[], sel=None, nInstMax=None):    
    BinnedHistoTask.__init__orig(self,name)
    self.m_phaseSpace = bps
    for i,expr in enumerate(calcExpr):
        c = jitConfig.buildCalculator(expr)
        self.setCalc(i,c)
    if sel:
        self.m_sel =  jitConfig.buildCalculator(sel)
    self.py_hbfillers = []
    if nInstMax is not None:
        self.m_nMaxInst = nInstMax

def add(self, expr, hspec, name=None, sel=None, **args ):
    if name is None: name='binFiller'+str(len(self.py_hbfillers))
    klass=args.pop('calcBaseClass',None)
    index=args.pop('index', -1)
    hfiller = histoFillerFromExpr( expr, hspec, name=name, sel=sel, fixedIndex=index , calcBaseClass=klass,**args)
    hdesc = hfiller.hdesc
    if ':' in expr:
        hbfiller = HistoBinFillerTH2F()
        self.m_th2Histos.push_back(hbfiller)        
    else:
        hbfiller = HistoBinFillerTH1F()
        self.m_th1Histos.push_back(hbfiller)
    #hbfiller.m_filler = hfiller
    hbfiller.setHFiller( hfiller)    
    hbfiller.m_histos.setHistoDesc(hdesc)
    hbfiller.py_filler = hfiller
    hbfiller.dump()
    self.py_hbfillers.append( hbfiller ) # remember !
    hbfiller.m_histos.m_name = name
    setattr( self, name, hbfiller)
    hbfiller.a = 0
    return hbfiller

def saveHistos(self, f):
    f.cd()
    self.m_phaseSpace.Write(self.name())
    for hbf in self.py_hbfillers:
        hbf.m_histos.writeTo(f)

def histos(self):
    l = []
    for hbf in self.py_hbfillers:
        l.extend( list(hbf.m_histos.m_binContent) )
    return l

    
_decorateClass(BinnedHistoTask, [saveHistos, add, histos] )                 
_replaceClassFunc(BinnedHistoTask, __init__)


## **********************************************
## Augmenting HistoFiller
## **********************************************
def _buildHisto(self, name, hspec, hclass):
    h = hclass(name, name, *hspec)
    self.m_h = h
    self._py_h = h
    self._name = name
    
HistoFillerTH1F = ROOT.JITA.HistoFiller("TH1F")
def __init__(self, expr, hspec, name=None, calcBaseClass=None, **args):
    HistoFillerTH1F.__init__orig(self)

    self.m_calc = jitConfig.buildCalculator(expr,calcBaseClass)
    if name is None:
        name=expr

    self._buildHisto(name, hspec, ROOT.TH1F)

_replaceClassFunc(HistoFillerTH1F, __init__)
_decorateClass(HistoFillerTH1F, [_buildHisto] )                 


HistoFillerTH1FSel = ROOT.JITA.HistoFillerSel("TH1F")
def __init__(self, expr, hspec, name=None, sel=None, calcBaseClass=None,**args):
    HistoFillerTH1FSel.__init__orig(self)

    self.m_calc = jitConfig.buildCalculator(expr,calcBaseClass)
    if name is None:
        name=expr
    #    h = ROOT.TH1F(name, name, *hspec)
    self._buildHisto(name, hspec,ROOT.TH1F)
    self.m_sel = jitConfig.buildCalculator(sel)

_replaceClassFunc(HistoFillerTH1FSel, __init__)
_decorateClass(HistoFillerTH1FSel, [_buildHisto] )                 


HistoFillerTH2F = ROOT.JITA.HistoFiller("TH2F")
def __init__(self, expr1, expr2, hspec, name=None, **args):
    HistoFillerTH2F.__init__orig(self)

    self.m_calc1 = jitConfig.buildCalculator(expr1)
    self.m_calc2 = jitConfig.buildCalculator(expr2)
    if name is None:
        name=expr2+'_vs_'+expr1
    self._buildHisto(name, hspec,ROOT.TH2F)

_replaceClassFunc(HistoFillerTH2F, __init__)
_decorateClass(HistoFillerTH2F, [_buildHisto] )                 

HistoFillerTH2FSel = ROOT.JITA.HistoFillerSel("TH2F")
def __init__(self, expr1, expr2, hspec, name=None, sel=None, **args):
    HistoFillerTH2FSel.__init__orig(self)

    self.m_calc1 = jitConfig.buildCalculator(expr1)
    self.m_calc2 = jitConfig.buildCalculator(expr2)
    if name is None:
        name=expr2+'_vs_'+expr1
    self._buildHisto(name, hspec,ROOT.TH2F)
    self.m_sel = jitConfig.buildCalculator(sel)

_replaceClassFunc(HistoFillerTH2FSel, __init__)
_decorateClass(HistoFillerTH2FSel, [_buildHisto] )                 
    

def histoFillerFromExpr(expr, hspec, name=None, fixedIndex=-1, sel=None, calcBaseClass=None, **histoArgs):
    exprNoNamespace = expr.replace('::', '_@@_') 
    if ':' in exprNoNamespace:
        expr2 , expr1 = exprNoNamespace.split(':')
        expr2 = expr2.replace('_@@_','::')
        expr1 = expr1.replace('_@@_','::')
        if sel is None:
            hfiller =  HistoFillerTH2F( expr1, expr2, hspec, name=name, calcBaseClass=calcBaseClass)
        else:
            hfiller =  HistoFillerTH2FSel( expr1, expr2, hspec, sel=sel,name=name,calcBaseClass=calcBaseClass)
    elif sel is None:
        hfiller =  HistoFillerTH1F( expr, hspec, name=name, calcBaseClass=calcBaseClass)
    else:
        #print "cccccccccccc Instantiating HistoFillerTH1FSel"
        hfiller =  HistoFillerTH1FSel( expr, hspec, name=name, sel=sel , calcBaseClass=calcBaseClass)
    hfiller.hdesc = HistoDesc( **histoArgs)        
    hfiller.m_fixedInst = fixedIndex
    # setHistoProp( hfiller.m_h , hfiller.hdesc.styleDict) useless : histos styles are not saved
    return hfiller


## **********************************************
## Augmenting HistoBinFiller
## **********************************************
HistoBinFillerTH1F = ROOT.JITA.HistoBinFiller("TH1F")
HistoBinFillerTH2F = ROOT.JITA.HistoBinFiller("TH2F")


    




## **********************************************
## Augmenting MainLoop
## (the instance is not shared with C++, so we can just derive instead of augmenting)
## **********************************************
class MainLoop(ROOT.JITA.MainLoop):
    
    _py_tasks = [] # we use it to keep python instances alive with their dynamic attributes
    def add(self, tool ):
        self._py_tasks.append(tool)
        self.m_tasks.push_back(tool)

        setattr(self, tool.name(), tool )


    def addHisto(self, expr, hspec, sel=None, name=None, **args):
        jitConfig.setTree( self.m_chain )
        if name is None:
            name = expr
        task = ROOT.JITA.HistoTask( name, expr,  hspec, sel=sel,  **args)
        self.add(task)

    def tasks(self):
        return self._py_tasks


    def saveOutput(self, f):
        from cPickle import dumps
        doClose = False
        if isinstance(f, str):
            f = ROOT.TFile( f , "recreate" )
            doClose = True
        f.cd()
        # for each task ...
        for task in self.tasks():
            print ' saving ', task.name()
            task.saveHistos(f)
        print "Saving output in ", f.GetName()
        if doClose: f.Close()

    def allHistos(self):
        l = []
        for task in self.tasks():
            l+= task.histos()
        return l

    def setEvent(self, evt):
        jitConfig.eventClass = evt.__class__.__name__
        self.m_event = evt
