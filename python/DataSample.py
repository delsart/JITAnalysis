##################################################
## DataSamples.py
##
## Utilities to manage samples and files
##
## DataSetDesc : describes a grid dataset (files location on disk, xsec, generator efficiency,...)
## DataSample : describes a set of datasets in a logical sample (ex: QCD sample ={J2,J3,J4,...}, or QCD sample for QGSP_BIC variation)
##              contain a list of DataSetDesc instances.
##
## DataSample (and DataSetDesc) objects can be cloned (ex : to describe variations)
## 
##############################################3

import ROOT
import glob
import os.path

class Common(object):
    globalInputDir=""

class DataSample(Common):
    alias = None
    desc = ''
    sampleType = "Nominal" # Nominal, QGSP, data...
    
    basedir = ""  
    filePattern = '__filePattern_not_defined__' # pattern in the input file name

    datasets = None

    totalLumi = 20000
    
    def __init__(self, alias, dsList, **args):
        self.datasets = dsList
        self.alias = alias
        for k, v in args.iteritems():
            setattr(self, k, v)

        #self.applyPropToDS()


    def setupMainLoop(self, mL):
        for ds in self.datasets:
            ds.retrieveInitEventNum()
            w = ds.scaleForLumi( self.totalLumi)
            for f in ds.files():
                mL.addFile( f, w)

    def histoFileName(self, tag):
        base= os.path.join(self.globalInputDir, "Histos", self.alias)
        return base+tag+'.root'

    def getChain(self, name="CollectionTree"):
        c = ROOT.TChain(name)
        for ds in self.datasets:
            for f in ds.files():
                c.Add(f)
        return c

class DataSetDesc(Common):
    xsec = 0  #
    eff = 1
    
    nEventInit = None # calculated by retrieveInitEventNum()
    
    parentSample = None

    def __init__(self,  alias='', localInputDir='', pattern='',   **args):
        self.alias = alias
        self.localInputDir = localInputDir
        self.desc = args.pop('desc',alias)
        self.pattern = pattern
        for k,v in args.iteritems():
            setattr(self, k, v )

    def files(self):
        filePatt = os.path.join(self.globalInputDir,self.localInputDir,self.pattern)
        return glob.glob(filePatt)

    def weight(self):
        return 1.

    def retrieveInitEventNum(self):
        if self.nEventInit :
            return

        ## For now we just assume the sum number of init event is
        ## stored in file beside the merged inputs (one file per DataSetDesc)
        f=self.files()[0] # assume there is only 1 merged file  
        from cPickle import load
        from os.path import dirname
        d = load( open(dirname( f )+'/nEvents.txt') )
        self.nEventInit = d

        ## if xAOD with Metadata something like below could work.
        ## n=0
        ## for f in self.files() :
        ##     print 'AAAA ',f
        ##     tf = ROOT.TFile(f)
        ##     m = tf.Get("myMetaData")
        ##     if not m:
        ##         m = tf.Get("MetaData")

        ##     else: # bug!!!
        ##         #m.Draw("nWeightedEvents:Length$(nWeightedEvents)","","goff")
        ##         m.Draw("nEvents:Length$(nEvents)","","goff")
        ##         v1=m.GetV1()
        ##         N=int(m.GetV2()[0])
        ##         print '_____ ',v1
        ##         if N==2:
        ##             n+=v1[0]
        ##             print 'adding ninitevent= ',v1[0]
        ##         else: # bugged MiniSkim
        ##             for i in range(2,int(m.GetV2()[0])):
        ##                 if((i+1)%3)==0:
        ##                     print '   adding ',v1[i]
        ##                     n+=v1[i]
        ## self.nEventInit = n
        
    def scaleForLumi(self, lumi):
        """Return the scale factor to apply to each event in this sample so it contains N = xsec*lumi """
        if lumi is None or self.xsec == 0 or self.nEventInit==0:
            return 1
        # return lumi*self.xsec / self.nEventInit
        return lumi*self.xsec*self.eff / self.nEventInit

    def lumiFromNumEvents(self):
        """Return the lumi corresponding to this sample raw number of events """
        if self.xsec == 0:
            return 1
        return  self.nEventInit / self.xsec

