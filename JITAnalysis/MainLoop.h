// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_MAINLOOP_H
#define JITANALYSIS_MAINLOOP_H

#include "JITAnalysis/AnalysisTask.h"
#include <vector>
#include "TChain.h"

#ifndef CUSTOM_READER
class TTreeReader;
#endif

namespace JITA {

  ///******************************
  /// \class MainLoop
  /// Implements a simple event loop over a TChain.
  /// Holds a list of AnalysisTask. Each task processEvent is called on each event.

  class MainLoop  {
  public:
    
    MainLoop();

    // adds a File to the input TChain, assiciating a sample weight (typically : xsection weight).
    void addFile(const std::string& f, double w=1.0);

    // Runs the event loop : initialize() , [ process() on each event] , finalize()
    int loop(int num=-1, int firstEvt=0);

    void initialize(RunInfo & r); // called once before entering the event loop
    void process(); // called at each event
    void finalize() ;
    
    
    /// remove all associated file. Calls m_chain.Reset()
    void reset();
    void dump();


    
    std::vector<AnalysisTask*> m_tasks;
    EventInfo *m_event=0;
    RunInfo   m_runInfo;
    
    TChain m_chain;
    std::vector<double> m_fileWeights;
  };

}

#endif
