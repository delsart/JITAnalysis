// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_ANALYSISTASK_H
#define JITANALYSIS_ANALYSISTASK_H

#include <string>
#include <set>


class TTree;
class TFile;


namespace JITA {
  class CalculatorBase;
  class TreeReader;

  ///******************************
  /// \class EventInfo
  ///  Event level information. An instance of such object is shared amongst all AnalysisTask and CalculatorBase
  /// (In more advanced analyis derived users implement derived classes of this, and share their derived EventInfo
  ///   instead, including in their JIT-compiled CalculatorBase)
  struct EventInfo {
    float sampleWeight = 1.;
    float totalWeight = 1.;
    int eventIndex;
    int eventInTreeIndex;
    int treeIndex;
  };

  
  ///******************************
  /// \class RunInfo
  /// Run-level information. An instance of this class is circualted amongst tasks during initialization
  ///  each AnalysisTask is expected to add its own CalculatorBase to the calculators set.
  struct RunInfo {
    EventInfo * event = 0;

    TreeReader *reader;

    std::set<CalculatorBase*> calculators; 

  };

  

  ///******************************
  /// \class AnalysisTask
  /// Implements a task to be performed on each event.
  /// 
  class AnalysisTask {
  public:

    AnalysisTask(): m_name("unnamed"){}
    AnalysisTask(const std::string & n) : m_name(n) {}
    virtual void initialize( RunInfo& ) {};
    virtual void initEvent( ) {}
    virtual void processEvent( ) {};

    virtual void finalize() {};
    virtual void writeTo(TFile*) {};

    std::string  name() {return m_name;}

    virtual ~AnalysisTask(){}

    EventInfo  * m_event;

    std::string m_name="";
  };


}
#endif
