// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_HISTOFILLERS_H
#define JITANALYSIS_HISTOFILLERS_H

#include "JITAnalysis/CalculatorBase.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TTreeReaderArray.h"

namespace JITA {

  /////////////////////////////////////////////
  /// \class HistoFillerBase
  /// Base class for HistoFillers. 
  /// HistoFillers encapsulate the filling details of TH1 (or TH2) with 1 (2) CalculatorBase objects.
  ///
  class HistoFillerBase {
  public:
    virtual ~HistoFillerBase(){}
    virtual void fillHisto(float ) {};
    virtual void initEvent() {}
    virtual void initialize( RunInfo& ) =0;
  };

  template <class T>
  class HistoFillerT : public HistoFillerBase {};

  /////////////////////////////////////////////
  /// \class HistoFillerT<TH1F>
  /// HistoFiller specialization of filling methods for TH1F using 1 CalculatorBase
  template <>
  class HistoFillerT<TH1F> : public HistoFillerBase {
  public:

    typedef TH1F hist_t;

    virtual ~HistoFillerT(){}

    virtual void fill(TH1F*h, int i, float w=1){
      //std::cout << m_calc << " TH1 filler "<< i << "   "<< h <<  std::endl;
      h->Fill(m_calc->calculate( i ) , w);
    }    

    virtual void initialize( RunInfo& r) {
      m_calc->initialize(r);
    }

    virtual TH1F* histo() {return 0;} // return a TH1 in case we hold one (implemented in concrete class below)


    //virtual void initEvent() {m_calc->initEvent();} not needed calcularors are initEvent from Mainloop

    size_t numInstances(){return m_calc->numInstances();}

    CalculatorBase *m_calc=nullptr; 
    
  };

  /////////////////////////////////////////////
  /// \class HistoFillerT<TH2F>
  /// The  HistoFiller specialization of filling methods for TH2F using 2 CalculatorBase
  template <>
  class HistoFillerT<TH2F> : public HistoFillerBase {
  public:
    typedef TH2F hist_t;

    virtual void fill(TH2F*h, int i, float w=1){
      h->Fill(m_calc1->calculate( i ), m_calc2->calculate( i ) , w);
    }    

    virtual void initialize( RunInfo& r) {
      m_calc1->initialize(r);
      m_calc2->initialize(r);
    }

    virtual TH2F* histo() {return 0;} // return a TH2 in case we hold one (implemented in concrete class below)


    size_t numInstances(){return TMath::Min(m_calc1->numInstances(), m_calc2->numInstances());}

    CalculatorBase *m_calc1=nullptr;
    CalculatorBase *m_calc2=nullptr; 
  };

  
  /////////////////////////////////////////////
  /// \class HistoFiller
  /// The concrete HistoFiller class. 
  /// Holds an histogram and fills it with its CalculatorBase instance(s).
  /// m_fixedInst member controls how the histo is filled:
  ///  - m_fixedInst==-1 : all the instances in the CalculatorBase(s) are used 
  ///  - m_fixedInst>=0  : only the instance evaluated at m_fixedInst is used to fill the histo.
  /// (note : this is irrelevant if the CalculatorBase is a pure scalar)
  ///
  template<class BASE> 
  class HistoFiller : public HistoFillerT<BASE> {
  public:
    typedef typename HistoFillerT<BASE>::hist_t hist_t;

    ~HistoFiller(){ std::cout << " deleting "<< this << std::endl;}
    using HistoFillerT<BASE>::fill;
    void fill(int i, float w=1.){ HistoFillerT<BASE>::fill(m_h,i,w) ;}

    void fillHisto(float w=1.){
      int num = this->numInstances();
      if(m_fixedInst>-1){ if(m_fixedInst<num) HistoFillerT<BASE>::fill(m_h,m_fixedInst,w); }
      else{
        //std::cout << " filling at "<< this<< "   "<< m_h << "  "<< num << std::endl;
        for(int i=0;i<num;i++) HistoFillerT<BASE>::fill(m_h,i,w);
      }     
    }



    virtual hist_t * histo(){return m_h;}

    
    hist_t* m_h = nullptr;
    int m_fixedInst=-1;
  };



  /////////////////////////////////////////////
  /// \class HistoFillerSel
  /// Same as HistoFiller but with an additionnal CalculatorBase which acts as a selector.
  /// The histogram is filled only if m_sel->calculate(i) evaluates to true.
  template<class BASE> 
  class HistoFillerSel : public HistoFillerT<BASE> {
  public:
    typedef typename HistoFillerT<BASE>::hist_t hist_t;


    virtual void initialize( RunInfo& r) {
      HistoFillerT<BASE>::initialize(r);
      m_sel->initialize(r);
    }

    //using HistoFillerT<BASE>::fill;
    virtual void fill(hist_t *h, int i, float w=1.){ 
      if( bool(m_sel->calculate( i ) )  ) HistoFillerT<BASE>::fill(h,i,w) ;
    }

    void fill(int i, float w=1.){ 
      if( bool(m_sel->calculate( i ) )  ) HistoFillerT<BASE>::fill(m_h,i,w) ;
    }

    void fillHisto(float w=1.){
      int num = TMath::Min( this->numInstances(), m_sel->numInstances());
      if(m_fixedInst>-1){ if(m_fixedInst<num) if( bool(m_sel->calculate( m_fixedInst ) ) ) HistoFillerT<BASE>::fill(m_h,m_fixedInst,w); }
      else{        
        for(int i=0;i<num;i++) if( bool(m_sel->calculate( i ) ) ) HistoFillerT<BASE>::fill(m_h,i,w);
      }     
    }

    virtual hist_t * histo(){return m_h;}

   
    hist_t* m_h = nullptr;
    int m_fixedInst=-1;

    CalculatorBase * m_sel;
  };


  

  /////////////////////////////////////////////
  /// \class HistoTask
  /// A simple AnalysisTask which fills a single histo using a HistoFillerBase
  /// 
  class HistoTask : public AnalysisTask {
  public:

    HistoTask(const std::string n="histotask") : AnalysisTask(n) {}

    virtual void initialize( RunInfo& r ) {m_filler->initialize(r);};

    virtual void initEvent() {m_filler->initEvent();}

    virtual void processEvent( ) {m_filler->fillHisto(m_event->totalWeight);};
    
    HistoFillerBase *m_filler = nullptr;
  };
}

#endif
