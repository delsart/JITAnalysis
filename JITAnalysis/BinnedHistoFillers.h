// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_BINNEDHISTOFILLERS_H
#define JITANALYSIS_BINNEDHISTOFILLERS_H

#include "JITAnalysis/HistoFillers.h"
#include "BinnedPhaseSpace/BinnedPhaseSpace.h"
#include "BinnedPhaseSpace/BinnedHistos.h"

#include <string>

namespace JITA {


  /////////////////////////////////////////////
  /// \class HistoBinFiller
  /// 
  /// HistoBinFiller fills histograms for each bin in a BinnedPhaseSpace. 
  /// Holds 
  ///  * a HistoFiller<T> to perform the filling operations
  ///  * a BinnedHistos<T> to contain each histogram per bin (and save them conveniently in an outputfile).
  ///
  template<class HIST>  
  class  HistoBinFiller  {
  public:
    typedef HIST hist_t;

    ~HistoBinFiller(){ std::cout << " DDDDDDDDDD delte HistoBinFiller "<< this << std::endl;}
    void initialize(RunInfo& r, BPS::BinnedPhaseSpace * ps){
      m_histos.initialize(ps,m_filler->histo());
      m_filler->initialize(r);
    }
    
    void fill(int bindex, int instance, float w) {
      m_filler->fill( m_histos[bindex] , instance, w );
    }

    void setHFiller( HistoFillerT<hist_t>* f){m_filler= f;}
   
    void dump(){std::cout << "HistoBinFiller "<< this << " hfiller= "<< m_filler << "  binHistos="<<&m_histos<<std::endl;}
 
    BPS::BinnedHistos<hist_t> m_histos;
    HistoFillerT<hist_t> *m_filler;
  };

  
  
  
  
  /////////////////////////////////////////////
  /// \class BinnedHistoTask
  /// A class dedicated to fill histograms classified according to a BinnedPhaseSpace
  /// Holds 
  ///   * a BinnedPhaseSpace instance 
  ///   * vector< HistoBinFiller< TH1/2F>* > one entry for each type of histo to be filled.
  ///   * 4 CalculatorBase instances each representing one dimenstion of the BinnedPhaseSpace
  ///   * 1 CalculatorBase instance to represent a selection : instance i is filled in the binned histos only if m_sel->calculate(i) evaluates to true
  ///   * m_fixedInst : if -1 will process all instances available in the calculators
  class BinnedHistoTask : public AnalysisTask {
  public:

    BinnedHistoTask(const std::string& n="histotask") : AnalysisTask(n) {}

    virtual void initialize( RunInfo& r );

    virtual void initEvent() ;

    virtual void processEvent( ) ;

    virtual size_t numInstances();
    
    int findInstanceBin( int i );

    
    void setCalc(int i, CalculatorBase*);

    BPS::BinnedPhaseSpace m_phaseSpace;
    std::vector< HistoBinFiller<TH1F>* > m_th1Histos;
    std::vector< HistoBinFiller<TH2F>* > m_th2Histos;


    // pyroot hates these : hide them and have accessor
    CalculatorBase* m_calc1 = 0;
    CalculatorBase* m_calc2 = 0;
    CalculatorBase* m_calc3 = 0;
    CalculatorBase* m_calc4 = 0;
    CalculatorBase* m_sel =0;


    int m_nMaxInst = 10000000 ;
    int m_fixedInst = -1  ; // not used yet
    
  protected:


  };
}

#endif
