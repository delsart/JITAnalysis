// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_READERMAP_H
#define JITANALYSIS_READERMAP_H

#include "JITAnalysis/AnalysisTask.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"

#include <map>

class TTreeReader;
namespace JITA {

  ////////////////////////////////////////////////
  /// \class ReaderMapT 
  /// A map<string, T*> where T is a TTreeReaderValue or TTreeReaderArray
  /// Simply adds 2 convenience methods to the map : get() & fillVectors()
  ///
  template <typename T>
  class ReaderMapT {
  public:
    
    void fillVectors(const std::vector<std::string> &vNames, std::vector<T*> & vReaders){
      vReaders.reserve(vNames.size());
      for(const std::string &name:vNames) vReaders.push_back( get(name) );
    }

    T* get(const std::string & key){
      auto it = m_map.find (key);
      if( it != m_map.end() ) return it->second;
      T* read = new T(*m_reader, key.c_str() );
      m_map[key] = read;
      return read;
    }

    TTreeReader *m_reader;
    std::map<std::string, T*> m_map;
  };

  ////////////////////////////////////////////////
  /// \class ReaderMap
  /// A central map to keep track and ensure uniqueness of each TTreeReaderValue or TTreeReaderArray
  /// used on a TTree (ROOT 6.08 seems buggy when reader are not unique).
  /// Holds a ReaderMapT for each supported type.
  ///
  class ReaderMap {
  public:

    ReaderMap(){};
    ReaderMap(TTreeReader *r){initialize(r);};

    virtual int initialize(TTreeReader *r);

    ReaderMapT<TTreeReaderArray<float> > & vecFloatMap(){ return m_afMap;}
    ReaderMapT<TTreeReaderArray<int> >   & vecIntMap(){ return m_aiMap;}
    ReaderMapT<TTreeReaderValue<int> >   & intMap(){ return m_siMap;}
    ReaderMapT<TTreeReaderValue<float> > & floatMap(){ return m_sfMap;}
    
  protected:
    
    ReaderMapT<TTreeReaderArray<float> > m_afMap;
    ReaderMapT<TTreeReaderArray<int> >   m_aiMap;
    ReaderMapT<TTreeReaderValue<int> >   m_siMap;
    ReaderMapT<TTreeReaderValue<float> > m_sfMap;
  };
  

}

#endif
