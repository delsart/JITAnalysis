// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_CALCULATORBASE_H
#define JITANALYSIS_CALCULATORBASE_H

#include <vector>
#include "JITAnalysis/AnalysisTask.h"

#define CUSTOM_READER
#ifndef CUSTOM_READER
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#else
#include "JITAnalysis/TreeReader.h"
#endif

class TTreeReader;
namespace JITA {

  ///******************************
  /// \class CalculatorBase
  /// Base class for calculators : an object implementing 'float calculate()'. 
  /// It holds several vector<TTreeReaderArray<x>* > to access data in a TTree.
  ///
  /// This base class is used with the 'JITBuilder' class within the config system:
  /// given a C++ expresion 'JITBuilder' will generate a class inheriting CalculatorBase.
  /// Each branch name in the expression is replaced by an entry in one of the vector<TTreeReaderArray<x>*> 
  /// defined in this base class. Then the expression is injected in the 'calculate()' method 
  /// of the generated daughter class.
  /// Finally gInterpreter.LoadText( ) is called on the generated class to JIT compile it and make 
  /// it available.
  ///
  /// Suported TTree branch types : unsigned int, int, float, vector<int>, vector<float>.
  class CalculatorBase {
  public:

    virtual ~CalculatorBase(){  }
    virtual float calculate(int ) { return 0.; }

    virtual void initEvent() { m_nInstance = m_isPureScalar ? 1 : -1; } 

    /// return min( array.size() for each array)
    virtual size_t numInstances(){
      if(m_nInstance>-1) return size_t(m_nInstance);
      size_t ni=9999999999;
      for(auto * a : m_af) {size_t s = a->size();if(s<ni) ni=s;} //std::cout << "xx "<< a->GetBranchName() << "  size="<< s<< '\n';
      for(auto * a : m_ai) {size_t s = a->size();if(s<ni) ni=s;}
      for(auto * a : m_ad) {size_t s = a->size();if(s<ni) ni=s;}
      m_nInstance= ni;
      //std::cout << this << "  numInstances = "<< ni << std::endl;
      return ni;
    }
    
    bool pureScalar(){return m_isPureScalar;}
    void setPureScalar(bool b){m_isPureScalar=b;}

    virtual void initialize(RunInfo &r);
    std::vector<TreeVariable<std::vector<float> >*>  m_af;
    std::vector<TreeVariable<std::vector<double> >*> m_ad;
    std::vector<TreeVariable<std::vector<int> > *>   m_ai;
    std::vector<TreeVariable<int>*>    m_si;
    std::vector<TreeVariable<float>*>  m_sf;
    std::vector<TreeVariable<double>*> m_sd;

    std::vector<std::string> m_afBrNames;
    std::vector<std::string> m_aiBrNames;
    std::vector<std::string> m_siBrNames;
    std::vector<std::string> m_sfBrNames;
    std::vector<std::string> m_adBrNames;
    std::vector<std::string> m_sdBrNames;
    
    int m_nInstance=-1;

    bool m_isPureScalar;

  };
  
}

#endif
