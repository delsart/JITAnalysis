// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_EVENTWEIGHTTASK_H
#define JITANALYSIS_EVENTWEIGHTTASK_H

#include "JITAnalysis/AnalysisTask.h"
#include "JITAnalysis/TreeReader.h"

namespace JITA {

  ///******************************
  /// \class EventWeightTask
  /// Implements a task which scale the event weight from a branch in the TTree
  /// 
  class EventWeightTask : public AnalysisTask {
  public:

    EventWeightTask(): AnalysisTask("EventWeightTask"){}
    EventWeightTask(const std::string & n, const std::string brName) : AnalysisTask(n) ,m_weightBrName(brName) {}

    virtual void initialize( RunInfo& );
    virtual void processEvent( ) ;

    std::string m_weightBrName="";

  protected:
    TreeVariable< float > *m_weights = 0;
  };

  ///******************************
  /// \class EventWeightTaskVec
  /// Implements a task which scale the event weight from a branch in the TTree
  /// same as above but using vector<float> branch
  class EventWeightTaskVec : public AnalysisTask {
  public:

    EventWeightTaskVec(): AnalysisTask("EventWeightTaskVec"){}
    EventWeightTaskVec(const std::string & n, const std::string brName) : AnalysisTask(n) ,m_weightBrName(brName) {}

    virtual void initialize( RunInfo& );
    virtual void processEvent( ) ;

    std::string m_weightBrName="";

  protected:
    TreeVariable< std::vector<float> > *m_weights = 0;
    int m_index = 0;
  };


}
#endif
