// Dear emacs, this is -*- c++ -*-
#ifndef JITANALYSIS_TREEREADER_H
#define JITANALYSIS_TREEREADER_H

#include <map>
#include <iostream>
#include <vector>
#include "TStreamerInfo.h"
#include "TStreamerElement.h"
#include "TTree.h"
#include "TBranchElement.h"
#include "TLeaf.h"
#include "JITAnalysis/AnalysisTask.h"

using std::cout;
using std::endl;
namespace JITA {

  
  ///******************************
  /// \class TreeVariableBase
  /// Base class for TreeVariable. Classes encapsulating access to data in TTree branches
  /// 
  ///
  class TreeVariableBase {
  public:
    
    TreeVariableBase(const std::string &n="_no_name_"  ): m_name(n){}
    virtual ~TreeVariableBase(){}
    virtual void setEntry(size_t i){ if(i==m_event) return; m_needRead = true; m_event=i;  }
    virtual bool changeTree(TTree* ){return false;}
    std::string name(){return m_name;}

    TBranch * m_br=0;
    bool m_needRead=true;
    size_t m_event=-1;
    std::string m_name;
  };


  ///******************************
  /// \class TreeVariable<T>
  /// Encapsulate access to data in simple type branches
  /// 
  template<class T>
  class TreeVariable : public TreeVariableBase{
  public:
    TreeVariable(const std::string &n="_no_name_" ): TreeVariableBase(n){}

    bool changeTree(TTree *t){
      t->SetMakeClass(1); // needed to be able to read individual branches within struct. Work only on split branches

      //std::cout << " changing tree for "<< this->name() << std::endl;
      t->SetBranchAddress(m_name.c_str(), &m_value, &m_br);
      m_needRead = true;
      m_event = -1;

      return true;
    }

    int read(){
      int i= m_br->GetEntry(m_event);
      m_needRead = false;
      return i;
    }

    T get(){if(m_needRead) read(); return m_value;}

    
    T m_value;
  };


  
  ///******************************
  /// \class TreeVariable< vector<T> >
  /// Encapsulate access to data in simple vector<type> branches
  /// 
  template<class T>
  class TreeVariable<std::vector<T> > : public TreeVariableBase{
  public:
    TreeVariable(const std::string &n="_no_name_" ): TreeVariableBase(n){}

    bool changeTree(TTree *t){
      //std::cout << " changing tree for "<< this->name() << std::endl;
      TBranchElement *el= dynamic_cast<TBranchElement*>(t->GetBranch(m_name.c_str()));
      if(el==nullptr){
        std::cout <<" TreeVariable vector . ERROR variable "<< m_name << "  not found in TTree "<< t->GetName() << std::endl;
        return false;
      }
      t->SetMakeClass(1); // needed to be able to read individual branches within struct. Work only on split branches
      m_vptr = &m_value;

      if( el != el->GetMother() )  { 
        // The case when the branch is part of a struct..
        t->SetBranchAddress(m_name.c_str(), &m_value, &m_br);
      }else{ 
        // we need to use the pointer to the branch... 
        t->SetBranchAddress(m_name.c_str(), &m_vptr, &m_br);
      }

      //std::cout << " Unsplit branch changing tree for  "<< this->name() << "  == "<< m_vptr << " ___ "<< &m_value << " __ "<< (void*)el->GetObject() << " "<< std::endl;
      
      //el->GetEntry(0);
      // Long_t offset = ((TStreamerElement*)el->GetInfo()->GetElements()->At(el->GetID()))->GetOffset();
      // std::cout << (Long_t) el->GetObject()<< " ____ offset "<< offset << std::endl;
      // m_vptr = (std::vector<T>*) (el->GetObject()+offset) ;

      m_needRead = true;
      m_event = -1;

      return true;
    }

    int read(){
      int i= m_br->GetEntry(m_event);
      m_needRead = false;
      return i;
    }

    size_t size(){if(m_needRead) read(); return m_value.size();}
    T get(){if(m_needRead) read(); return m_value[0];}
    T get(int i){if(m_needRead) read(); return m_value[i];}
    T operator[](size_t i){return get(i);}
    
    std::vector<T> m_value;
    std::vector<T>* m_vptr=0;

  };
  

  
  ///******************************
  /// \class TreeReader
  /// Map, create and read TreeVariable associated to a given TTree.
  /// 

  class TreeReader {
  public:
    TreeReader(TTree * tree) : m_tree(tree){};

    void setEntry(size_t i);

    template<typename T>
    TreeVariable<T>* getVariable(const std::string& n){
      TBranch* br = m_tree->GetBranch(n.c_str());
      if( br == nullptr) {
        std::cout << "TreeReader  ERROR  branch "<< n << " does not exist in TTree "<< m_tree->GetName() << std::endl;
        return nullptr;
      }
      auto it = m_allVars.find(n);
      if( it != m_allVars.end() ) return dynamic_cast< TreeVariable<T>* >( it->second ) ;
      TreeVariable<T>* v = new TreeVariable<T>(n);
      m_allVars[n] =v;      
      return v;
    }

    bool assignVariable(TreeVariableBase *v);
    void changeTree();

    void SetEntry(size_t i){setEntry(i);}
    
    TTree* m_tree;
    int m_lastTreeNum=-1;
  protected:
    std::map<std::string, TreeVariableBase*> m_allVars;
    
  };


}
#endif
