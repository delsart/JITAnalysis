JITAnalysis
===========

A minimal framework to analyze TTrees with JIT
(Just-In-Time) compiled expressions.

$`\alpha + \pi `$

The aim of this approach is to find a good balance between flexibility (most of
the analysis written in python config scripts) and speed (critical
parts are compiled C++).

Basic usage : histograms filling
------------------------------
The package is driven by this sort of pyROOT scripts :

```python
# prepare a loop over all event in a tree
mainL = MainLoop()
mainL.m_chain.SetName("CollectionTree") 
mainL.addFile( "somefile.root")

# add many histos to be drawn
mainL.addHisto( 'variable1', (500,0,3000000 ), name='histo_var1')
mainL.addHisto( 'variable2', (500,0,3000000 ), name='histo_var2')
mainL.addHisto( 'variable3', (500,-5, 5 ), name='histo_var3')

# any c++ expression is ok, including *any* function/code known to ROOT
mainL.addHisto( 'variable1+TMat::Log(variable2)+34', (500,0, 3000 ), name='histo_var1_logvar2')

# run 
mainL.loop()
# save :
mainL.saveOutput( "out.root" )
```
    
Content
------------------------------

It consists of 3 c++ main classes :
  * `AnalysisTask` represent a task to be performed on each
  event. Clients will inherit this base class and provide implementations.
  * `MainLoop` implement a basic loop over a TTree. Holds a vector of
  `AnalysisTask`, each are called on each events.
  * `CalculatorBase` a base class to perform a calculation returning a
  value. This base class is used by the config system to derive & JIT
  compile calculators from arbitrary expressions.

and a configuration module in python : `JITAnalysisConfig.py`. This
module augments the above C++ classes by various helper functions.




Usage example
=============
The package depends on BinnedPhaseSpace. 

``` 
# in case you don't have AnalysisBase, check out RootCore :
# svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/D3PDTools/RootCore/trunk RootCore
# do this to setup RootCore (rc comands)
# source RootCore/scripts/setup.sh
git clone https://:@gitlab.cern.ch:8443/delsart/BinnedPhaseSpace.git
git clone https://:@gitlab.cern.ch:8443/delsart/JITAnalysis.git

rc find_packages
rc compile 
cd JITAnalysis/scripts/
python demo.py

``` 

See `scripts/demo.py` and `scripts/demoExtended.py`
