#include <valgrind/callgrind.h>

#include "JITAnalysis/MainLoop.h"
#include "JITAnalysis/CalculatorBase.h"

//#include "xAODBase/IParticleContainer.h"
#include <iostream>
#include "TStopwatch.h"


namespace JITA {

  MainLoop::MainLoop() { }


  void MainLoop::initialize(RunInfo &r){
    
    for(AnalysisTask* t : m_tasks) {
      t->initialize(r);
      t->m_event = r.event;
    }
    std::cout << " MainLoop::initialize.  Num tasks="<< m_tasks.size()  << std::endl;
  }


  void MainLoop::process( ){

    //std::cout << " MainLoop::process.  initEvent calc ="<< m_runInfo.calculators.size()  << std::endl;
    for(CalculatorBase* c: m_runInfo.calculators){
      c->initEvent();
    }

    //std::cout << " MainLoop::process.  initEvent task ="  << std::endl;
    for(AnalysisTask* t: m_tasks){
      t->initEvent();
    }

    //std::cout << " MainLoop::process.  processEvent task ="  << std::endl;
    for(AnalysisTask* t: m_tasks){
      t->processEvent();
    }

  }

  void MainLoop::finalize(){
  }


  void MainLoop::reset(){
    m_chain.Reset();
    m_fileWeights.clear();
  }

  void MainLoop::addFile(const std::string& f, double w){ 
    m_chain.AddFile(f.c_str()); 
    m_fileWeights.push_back(w) ;
  }

  void MainLoop::dump(){
    for(AnalysisTask* t: m_tasks) {
      std::cout << " task "<< t->name() << "  _  "<< t << std::endl;
    }
  }
  
    
  int MainLoop::loop(int num, int firstEvt){

    TStopwatch clock;
    int last = num+firstEvt;

    if (num<0) {num = m_chain.GetEntries(); last = num;}

    int reportStep = (num/10) ;
    //int reportStep = 1;
    if( reportStep == 0) reportStep =1;
    //std::cout << "   LOOP  "<< num << "  report "<< reportStep<<std::endl;

    CALLGRIND_START_INSTRUMENTATION;
    int lastTreeNum = -1;

    m_chain.LoadTree(firstEvt);

    TreeReader reader(&m_chain);

    std::cout << "  ___ the reader ___ "<< &reader << std::endl;

    if(m_event==0) m_event = new EventInfo();
    m_runInfo.reader = &reader;
    m_runInfo.event  = m_event;

    initialize(m_runInfo);
    
    

    for(int i=firstEvt;i<last;i++){
      m_event->eventIndex = i;
      m_event->eventInTreeIndex = m_chain.LoadTree(i);
      reader.SetEntry( i );
      
      //std::cout << &m_event << " "<< m_runInfo.event << std::endl;
      
      if( lastTreeNum != m_chain.GetTreeNumber() ){
        lastTreeNum = m_chain.GetTreeNumber() ;
        m_event->sampleWeight = m_fileWeights[ lastTreeNum ];
      }
      
      m_event->totalWeight = m_event->sampleWeight;

            
      //std::cout << " ev "<< i << std::endl;
      process();
      
      if( (i%reportStep)==0 )std::cout<< i+1<< "/"<<num<<"  entries processed "<< std::endl;
    }

    CALLGRIND_STOP_INSTRUMENTATION;

    std::cout << "   Time = "<< clock.RealTime() << "   nEvts="<< last-firstEvt << "  -> "<< (last-firstEvt)/clock.RealTime()*0.001 << " kHz "<< std::endl;
    return num;
  }




  
}
