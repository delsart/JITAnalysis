#include "JITAnalysis/CalculatorBase.h"

#include "TTreeReader.h"

namespace JITA {

  void CalculatorBase::initialize(RunInfo & r){
    std::cout << " Initialize CalculatorBase "<< this<< "  reader "<< r.reader << std::endl;

    for(std::string &s: m_afBrNames) m_af.push_back( r.reader->getVariable< std::vector<float> >(s) );
    for(std::string &s: m_aiBrNames) m_ai.push_back( r.reader->getVariable< std::vector<int> >(s) );
    for(std::string &s: m_adBrNames) m_ad.push_back( r.reader->getVariable< std::vector<double> >(s) );
    for(std::string &s: m_sfBrNames) m_sf.push_back( r.reader->getVariable< float >(s) );
    for(std::string &s: m_siBrNames) m_si.push_back( r.reader->getVariable< int >(s) );
    for(std::string &s: m_sdBrNames) m_sd.push_back( r.reader->getVariable< double >(s) );

    r.calculators.insert( this );
    //m_isPureScalar =  (m_af.empty()&&m_ai.empty() ) ; must be set by configuration
  }


}
