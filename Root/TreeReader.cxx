#include "JITAnalysis/TreeReader.h"


namespace JITA {

  void TreeReader::setEntry(size_t i){
    size_t indexInTree = m_tree->LoadTree(i);

    // Check if we need to change tree ----------------
    if( m_lastTreeNum != m_tree->GetTreeNumber() ) changeTree() ;

    for(auto &it : m_allVars){
      TreeVariableBase* v = it.second;
      v->setEntry(indexInTree);
    }
  }

  void TreeReader::changeTree(){
    std::cout <<" TreeReader::changeTree change tree : "<< m_tree->GetTreeNumber() <<  "  "<<  m_allVars.size()<<std::endl;
    m_lastTreeNum = m_tree->GetTreeNumber();
    TTree* currentTree = m_tree->GetTree();
    if(! currentTree) { 
      m_lastTreeNum=-1;
      return;
    }
    //currentTree->SetMakeClass(1);
    for(auto & it : m_allVars) {
      //std::cout<< " change tree "<< it.second->m_name << std::endl;
      TreeVariableBase* v = it.second;
      bool ok = v->changeTree( currentTree );
      if( !ok ){
        std::cout<< m_lastTreeNum << " ERROR  entry   Branch : "<< v->name() << " could not be set "<< std::endl;
        return;
      }
    } 
  }
  
  bool TreeReader::assignVariable(TreeVariableBase *v){
    auto it = m_allVars.find(v->name());
    if( it != m_allVars.end() ) {
      std::cout << " ERROR : Variable v " << v->name() << " already assigned in TreeReader "<< this <<std::endl;
      return false;
    }
    m_allVars[v->name()] =v;
    return true;
  }



  template class TreeVariable<float> ;
  template class TreeVariable<double> ;
  template class TreeVariable<int> ;
  template class TreeVariable<unsigned int> ;
  template class TreeVariable<std::vector<float> > ;
  template class TreeVariable<std::vector<double> > ;
  template class TreeVariable<std::vector<int> > ;

  template TreeVariable<float>* TreeReader::getVariable<float>(const std::string &);
  template TreeVariable<int>* TreeReader::getVariable<int>(const std::string &);
  template TreeVariable<unsigned int>* TreeReader::getVariable<unsigned int>(const std::string &);
  template TreeVariable<std::vector<float> >* TreeReader::getVariable<std::vector<float> >(const std::string &);
  template TreeVariable<std::vector<int> >*   TreeReader::getVariable<std::vector<int> >(const std::string &);

}
