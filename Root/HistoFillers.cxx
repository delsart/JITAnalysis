#include "JITAnalysis/HistoFillers.h"



namespace JITA {

// explicit instantiation
  template class HistoFillerT<TH1F> ;
  template class HistoFillerT<TH2F> ;
  template class HistoFiller<TH1F> ;
  template class HistoFiller<TH2F> ;

  template class HistoFillerSel<TH1F> ;
  template class HistoFillerSel<TH2F> ;

}
