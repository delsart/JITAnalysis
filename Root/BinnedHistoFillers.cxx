#include "JITAnalysis/BinnedHistoFillers.h"


namespace JITA {


  void BinnedHistoTask::initialize( RunInfo& r ){
    for(HistoBinFiller<TH1F>* f: m_th1Histos){
      f->initialize(r, &m_phaseSpace);
    }
    for(HistoBinFiller<TH2F>* f: m_th2Histos){
      f->initialize(r, &m_phaseSpace);
    }      
    CalculatorBase * calcs[5] = {m_calc1,m_calc2,m_calc3,m_calc4,m_sel};
    for (CalculatorBase* c: calcs) { if(c ) c->initialize(r);}
  }
  
  void BinnedHistoTask::initEvent() {
  }

  size_t BinnedHistoTask::numInstances( ) {
    int max = 0;
    int max_tmp = 0;
    if(m_calc1) if(!m_calc1->pureScalar()) {max= m_calc1->numInstances(); } //std::cout<< m_calc1<< "  nInst="<<m_calc1->m_nInstance<< " calc1 ninst= "<< max << std::endl;}
    if(m_calc2) if(!m_calc2->pureScalar()) {max_tmp= m_calc2->numInstances(); max = max_tmp<max ? max_tmp : max;}
    if(m_calc3) if(!m_calc3->pureScalar()) {max_tmp= m_calc3->numInstances(); max = max_tmp<max ? max_tmp : max;}
    if(m_calc4) if(!m_calc4->pureScalar()) {max_tmp= m_calc4->numInstances(); max = max_tmp<max ? max_tmp : max;}
    if(m_sel) if(!m_sel->pureScalar()) {max_tmp= m_sel->numInstances(); max = max_tmp<max ? max_tmp : max;}

   
    return max < m_nMaxInst ? max : m_nMaxInst;
  }


  void BinnedHistoTask::processEvent( ) {

    initEvent();
    size_t nInst = numInstances();
    //std::cout << " BinnedHistoTask::process.  n instance ="  << nInst<< std::endl;

    if( (nInst==0) && (m_phaseSpace.nDim()==0) ) nInst++;
    float w = m_event->totalWeight;
    for(size_t i=0;i<nInst;i++){
      int b = findInstanceBin(i);
      if(b>-1) {        
        for(HistoBinFiller<TH1F>* f: m_th1Histos){
          f->fill(b,i,w);
        }
        for(HistoBinFiller<TH2F>* f: m_th2Histos){
          f->fill(b,i,w);
        }
      }
    }
  }


  int BinnedHistoTask::findInstanceBin( int i ){
    if(m_sel!=0){
      bool ok = bool( m_sel->calculate(i) );
      if( !ok) return -1;
    }
    switch(m_phaseSpace.nDim()){
    case 0: return 0;
    case 1: return m_phaseSpace.findBin( m_calc1->calculate(i) );
    case 2: return m_phaseSpace.findBin( m_calc1->calculate(i) , m_calc2->calculate(i) );
    case 3: return m_phaseSpace.findBin( m_calc1->calculate(i) , m_calc2->calculate(i), m_calc3->calculate(i) );
    case 4: return m_phaseSpace.findBin( m_calc1->calculate(i) , m_calc2->calculate(i), m_calc3->calculate(i) , m_calc4->calculate(i) );
    default: break ;
    }
    return 0; 
  }

  void BinnedHistoTask::setCalc(int i, CalculatorBase* c){
    switch(i){
    case 0: {m_calc1=c;break;}
    case 1: {m_calc2=c;break;}
    case 2: {m_calc3=c;break;}
    case 3: {m_calc4=c;break;}
    default: {std::cout << " !!! ERROR BinnedHistoTask::setCalc  at "<< i << std::endl;}
    }
  }


  template class HistoBinFiller<TH1F> ;
  template class HistoBinFiller<TH2F> ;
  

}
