#include "JITAnalysis/ReaderMap.h"
#include "TTreeReader.h"


namespace JITA {

  int ReaderMap::initialize(TTreeReader *r){
    m_afMap.m_reader = r ;
    m_aiMap.m_reader = r ;
    m_siMap.m_reader = r ;
    m_sfMap.m_reader = r ;

    return 0;
  }


}
