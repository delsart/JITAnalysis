#include "JITAnalysis/EventWeightTask.h"

namespace JITA {


  void EventWeightTask::initialize( RunInfo& r){
    m_weights = r.reader->getVariable<float>( m_weightBrName );
  }
  
  void EventWeightTask::processEvent( ) {
    m_event->totalWeight *= m_weights->get();
  }


  void EventWeightTaskVec::initialize( RunInfo& r){
    m_weights = r.reader->getVariable<std::vector<float> >( m_weightBrName );
  }
  
  void EventWeightTaskVec::processEvent( ) {
    m_event->totalWeight *= m_weights->get(m_index);
  }

}
