#include "JITAnalysis/AnalysisTask.h"
#include "JITAnalysis/CalculatorBase.h"
#include "JITAnalysis/HistoFillers.h"
#include "JITAnalysis/BinnedHistoFillers.h"
#include "JITAnalysis/MainLoop.h"
#include "JITAnalysis/EventWeightTask.h"

#include "JITAnalysis/TreeReader.h"


#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class JITA::EventInfo+;
#pragma link C++ class JITA::RunInfo+;
#pragma link C++ class JITA::AnalysisTask+;
#pragma link C++ class std::vector<JITA::AnalysisTask>;

#pragma link C++ class std::vector<TTreeReaderArray<float>*>;
#pragma link C++ class std::vector<TTreeReaderArray<int>*>;
#pragma link C++ class std::vector<TTreeReaderValue<float>*>;
#pragma link C++ class std::vector<TTreeReaderValue<int>*>;
#pragma link C++ class JITA::CalculatorBase+;


#pragma link C++ class JITA::HistoTask+;
#pragma link C++ class JITA::HistoFillerBase;
#pragma link C++ class JITA::HistoFillerT<TH1F>;
#pragma link C++ class JITA::HistoFillerT<TH2F>;
#pragma link C++ class JITA::HistoFiller<TH1F>;
#pragma link C++ class JITA::HistoFiller<TH2F>;

#pragma link C++ class JITA::HistoFillerSel<TH1F>;
#pragma link C++ class JITA::HistoFillerSel<TH2F>;


#pragma link C++ class JITA::HistoBinFiller<TH1F>;
#pragma link C++ class JITA::HistoBinFiller<TH2F>;
#pragma link C++ class std::vector< JITA::HistoBinFiller<TH1F>* >;
#pragma link C++ class std::vector< JITA::HistoBinFiller<TH2F>* >;
#pragma link C++ class JITA::BinnedHistoTask+;


#pragma link C++ class JITA::MainLoop+;
#pragma link C++ class JITA::EventWeightTask;
#pragma link C++ class JITA::EventWeightTaskVec;

#pragma link C++ class JITA::TreeVariableBase;
#pragma link C++ class JITA::TreeVariable<float>;
#pragma link C++ class JITA::TreeVariable<double>;
#pragma link C++ class JITA::TreeVariable<int>;
#pragma link C++ class JITA::TreeVariable<unsigned int>;
#pragma link C++ class JITA::TreeVariable<std::vector<float> >;
#pragma link C++ class JITA::TreeVariable<std::vector<double> >;
#pragma link C++ class JITA::TreeVariable<std::vector<int> >;
#pragma link C++ class JITA::TreeReader;
#pragma link C++ function JITA::TreeReader::getVariable<unsigned int>;
#pragma link C++ function JITA::TreeReader::getVariable<int>;
#pragma link C++ function JITA::TreeReader::getVariable<double>;
#pragma link C++ function JITA::TreeReader::getVariable<float>;
#pragma link C++ function JITA::TreeReader::getVariable<std::vector<float> >;
#pragma link C++ function JITA::TreeReader::getVariable<std::vector<double> >;
#pragma link C++ function JITA::TreeReader::getVariable<std::vector<int> >;



#endif
